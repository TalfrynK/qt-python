# -*- coding: utf-8 -*-
"""
Created on Wed Jan 22 14:55:40 2020

@author: avine
"""

import sys
from PySide2.QtWidgets import QLineEdit, QPushButton, QApplication, QVBoxLayout, QDialog

class Form(QDialog):
    
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        self.setWindowTitle("My Form")
        
        # Creating widgets
        self.edit = QLineEdit("Write your name here...")
        self.button = QPushButton("Show Greetings!")
        self.button.clicked.connect(self.greeting)
        
        # Creating a layout and adding/organising the widgets
        layout = QVBoxLayout()
        layout.addWidget(self.edit)
        layout.addWidget(self.button)
        
        # Setting the Form layout
        self.setLayout(layout)
        
    # Greets the user
    def greeting(self):
        print ("Hello {}".format(self.edit.text()))
        print ("Hello " + self.edit.text())
        print ("Hello %s" % self.edit.text())
        # All can be used
        
if __name__ == "__main__":
    # Creating the application
    app = QApplication(sys.argv)
    # Creating/Showing the Form
    form = Form()
    form.show()
    app.exec()