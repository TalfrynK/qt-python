# -*- coding: utf-8 -*-
"""
Created on Wed Jan 22 14:31:38 2020

@author: avine
"""

import sys
from PySide2.QtWidgets import QApplication, QLabel

app = QApplication(sys.argv)
label = QLabel("<font color=red size=40>Hello, World!</font>")
label.show()
app.exec_()