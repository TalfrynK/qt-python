# -*- coding: utf-8 -*-
"""
Created on Wed Jan 22 20:07:04 2020

@author: avine
"""

import sys
from PySide2.QtWidgets import QApplication, QMainWindow
from PySide2.QtCore import QFile
from ui_mainwindow_tut4_uifiles import Ui_MainWindow

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow() # Load the generated python file
        self.ui.setupUi(self) # setup the self which is the Ui_MainWindow
        
if __name__ == "__main__":
    app = QApplication(sys.argv)
    
    window = MainWindow()
    window.show()
    
    app.exec_()