# -*- coding: utf-8 -*-
"""
Created on Wed Jan 22 21:30:16 2020

@author: avine
"""

from PySide2.QtCore import Slot, qApp
from PySide2.QtGui import QKeySequence
from PySide2.QtWidgets import QMainWindow, QAction, QFileDialog


class MainWindow(QMainWindow):
    def __init__(self, widget):
        QMainWindow.__init__(self)
        self.setWindowTitle("Data Visualisation")
        self.setCentralWidget(widget)
        
        self.menu = self.menuBar()
        self.file_menu = self.menu.addMenu("File")
        
        # Exit QAction
        exit_action = QAction("Exit", self)
        exit_action.setShortcut(QKeySequence.Quit)
        exit_action.triggered.connect(self.close)
        
        # Open QAction
        load_action = QAction("Open File...", self)
        self.load = QFileDialog(self)
        load_action.triggered.connect(self.load.open)
        
        # Load data from File QAction
        data = self.load.getOpenFileName()

        
        self.file_menu.addAction(load_action)
        self.file_menu.addAction(exit_action)
        
        
        print(data)
        
        # Status Bar
        self.status = self.statusBar()
        self.status.showMessage("Data loaded and plotted")
        
        # Window Dimensions
        geometry = qApp.desktop().availableGeometry(self)
        self.setFixedSize(geometry.width() * 0.8, geometry.height() * 0.7)
        