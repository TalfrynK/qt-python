# -*- coding: utf-8 -*-
"""
Created on Wed Jan 22 20:15:10 2020

@author: avine
"""

import sys
from PySide2.QtUiTools import QUiLoader # Allows dynamic loading of the .ui file
from PySide2.QtWidgets import QApplication
from PySide2.QtCore import QFile

if __name__ == "__main__":

    app = QApplication(sys.argv)
    
    ui_file = QFile("mainwindow.ui") # Sets the .ui file name
    ui_file.open(QFile.ReadOnly) # Opens and sets the rules for the .ui file
    
    loader = QUiLoader()
    window = loader.load(ui_file) # Loads the opened .ui file
    window.show()
    
    app.exec_()