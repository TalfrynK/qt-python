# -*- coding: utf-8 -*-
"""
Created on Thu Jan 23 11:41:28 2020

@author: avine
"""

import sys
from PySide2.QtCore import Qt, Slot
from PySide2.QtGui import QPainter, QColor
from PySide2.QtWidgets import (QAction, QApplication, QHeaderView, QHBoxLayout, QLabel, QLineEdit,
                               QMainWindow, QPushButton, QTableWidget, QTableWidgetItem,
                               QVBoxLayout, QWidget, QMessageBox)
from PySide2.QtCharts import QtCharts
import pandas as pd
import os
        
        
class Widget(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self.items = 0
        self.popup = QMessageBox()
        self.colour = QColor()
        self.total = 0
        
        if os.path.exists("data.csv"):
            self.data = {}
            
            dfdata = pd.read_csv("data.csv")
            dfdata = pd.DataFrame.to_numpy(dfdata)
            print (dfdata)
            print(len(dfdata))
            
            for i in range(0, (len(dfdata))):
                curData = dfdata[i]
                curDataArray = list(curData)
                strlength = len(curDataArray)
                print (strlength)
                print(curData)
                print(curDataArray[0], curDataArray[1])
                key = curDataArray[0]
                value = float(curDataArray[1])
                self.data.update({key: value})

        else:
            self.data = {"Water": 24.5, "Electricity": 55.1, "Rent": 850.0,
                      "Supermarket": 230.4, "Internet": 29.99, "Bars": 21.85,
                      "Public transportation": 60.0, "Coffee": 22.45, "Restaurants": 120}
        
        self.price_temp = []
        self.desc_temp = []
        # Left 
        self.left = QVBoxLayout()
        self.left.setMargin(10)
        self.table = QTableWidget()
        self.table.setColumnCount(2)
        self.table.setHorizontalHeaderLabels(["Description", "Price"])
        self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.left.addWidget(self.table)
        
        self.deleteItem = QPushButton("Delete Item")
        self.edit = QPushButton("Edit Item")
        self.clear = QPushButton("Clear Table")        
        
        self.deleteItem.clicked.connect(self.delete_item)
        self.edit.clicked.connect(self.edit_item)

        # Chart
        self.chart_view = QtCharts.QChartView()
        self.chart_view.setRenderHint(QPainter.Antialiasing)
        
        # Right
        self.description = QLineEdit()
        self.price = QLineEdit()
        self.add = QPushButton("Add")
        self.quit = QPushButton("Quit")
        self.plot = QHBoxLayout()
        self.plot_pie = QPushButton("Plot Pie Chart")
        self.plot_bar = QPushButton("Plot Value Bar Chart")
        self.plot_bar_percent = QPushButton("Plot Percentage Bar Chart")
        self.save_table = QPushButton("Save Table")
        self.plot.addWidget(self.plot_pie)
        self.plot.addWidget(self.plot_bar)
        self.plot.addWidget(self.plot_bar_percent)
        
        self.add.setEnabled(False)
        
        self.right = QVBoxLayout()
        self.right.setMargin(10)
        self.right.addWidget(QLabel("Description"))
        self.right.addWidget(self.description)
        self.right.addWidget(QLabel("Price"))
        self.right.addWidget(self.price)
        self.right.addWidget(self.add)
        self.right.addWidget(self.chart_view)
        self.right.addLayout(self.plot)
        self.right.addWidget(self.save_table)
        self.right.addWidget(self.quit)
        
        # Widget Layout
        self.layout = QHBoxLayout()     
        self.layout.addLayout(self.left)
        self.layout.addLayout(self.right)
        self.setLayout(self.layout)
        
        # Signals and Slots
        self.add.clicked.connect(self.add_element)
        self.plot_bar.clicked.connect(self.plot_data_bar)
        self.plot_pie.clicked.connect(self.plot_data_pie)
        self.plot_bar_percent.clicked.connect(self.plot_percent_bar)
        self.save_table.clicked.connect(self.save_data)
        self.quit.clicked.connect(self.quit_application)
        self.clear.clicked.connect(self.clear_table)
        self.description.textChanged[str].connect(self.check_disable)
        self.price.textChanged[str].connect(self.check_disable)
        self.fill_table()
        
        for i in range(self.table.rowCount()):
            self.total += float(self.table.item(i, 1).text())
        
        self.total_display = QLabel("Total: " + str(self.total))
        
        self.left.addWidget(self.total_display)
        self.left.addWidget(self.edit)
        self.left.addWidget(self.deleteItem)
        self.left.addWidget(self.clear)        
        self.table.cellChanged.connect(self.update_total)
            

        
    @Slot()
    def update_total(self):
        self.total = 0
        for i in range(self.table.rowCount()):
            self.total += float(self.table.item(i, 1).text())
            
        self.total_display.setText("Total: " + str(self.total))
        
    @Slot()        
    def add_element(self):
        desc = self.description.text()
        price = self.price.text()
        
        if price.isdigit() == True:
            self.table.insertRow(self.items)
            description_item = QTableWidgetItem(desc)
            price_item = QTableWidgetItem("{:.2f}".format(float(price)))
            price_item.setTextAlignment(Qt.AlignRight)
                    
            self.table.setItem(self.items, 0, description_item)
            self.table.setItem(self.items, 1, price_item)
            
            self.price_temp.append(float(price))
            self.desc_temp.append(desc)
            
            self.description.setText("")
            self.price.setText("")
                        
            self.items += 1
            
            self.total += float(price)
            
            self.update_total()
        else:
            self.popup.warning(self, "Error", "Price input must be a numerical value.")
            return

        
    @Slot()
    def check_disable(self, s):
        if not self.description.text() or not self.price.text():
            self.add.setEnabled(False)
        else:
            self.add.setEnabled(True)
        
            
    @Slot()
    def plot_data_bar(self):
        series = QtCharts.QBarSeries() 
        self.axis_y = QtCharts.QValueAxis()
        self.axis_y.setTickCount(10)
        self.axis_y.setLabelFormat("%.2f")
        self.axis_y.setTitleText("Price")

        self.colour.black()
        for i in range(self.table.rowCount()):
            text = self.table.item(i, 0).text()
            series_set = QtCharts.QBarSet(text)
            series_set.setBorderColor(self.colour)
            number = float(self.table.item(i, 1).text())
            series_set.append(number)
            series.insert(round(number), series_set)
        chart = QtCharts.QChart()
        chart.addSeries(series)
        chart.legend().setAlignment(Qt.AlignBottom)
        chart.addAxis(self.axis_y, Qt.AlignLeft)
        series.attachAxis(self.axis_y)
        self.chart_view.setChart(chart)
        
    @Slot()
    def plot_data_pie(self):
        series = QtCharts.QPieSeries()
        for i in range(self.table.rowCount()):
            text = self.table.item(i, 0).text()
            number = float(self.table.item(i, 1).text())
            text = text + ": " + str(round(number/self.total * 100, 2)) + "%"
            series.append(text, number)
        chart = QtCharts.QChart()
        chart.addSeries(series)
        chart.legend().setAlignment(Qt.AlignLeft)
        self.chart_view.setChart(chart)
        
    @Slot()
    def plot_percent_bar(self):
        series = QtCharts.QBarSeries() 
        self.axis_y = QtCharts.QValueAxis()
        self.axis_y.setTickCount(10)
        self.axis_y.setLabelFormat("%.2f" + "%")
        self.axis_y.setTitleText("Price Percent")

        self.colour.black()
        
        for i in range(self.table.rowCount()):
            text = self.table.item(i, 0).text()
            series_set = QtCharts.QBarSet(text)
            series_set.setBorderColor(self.colour)
            number = float(self.table.item(i, 1).text())
            number = float(round(number/self.total, 2))
            series_set.append(number)
            series.insert(round(number), series_set)
        chart = QtCharts.QChart()
        chart.addSeries(series)
        chart.legend().setAlignment(Qt.AlignBottom)
        chart.addAxis(self.axis_y, Qt.AlignLeft)
        series.attachAxis(self.axis_y)
        self.chart_view.setChart(chart)    
        
    @Slot()
    def quit_application(self):
        QApplication.quit()
        
    def fill_table(self, data=None):
        data = self.data if not data else data
        for desc, price in data.items():
            description_item = QTableWidgetItem(desc)
            price_item = QTableWidgetItem("{:.2f}".format(price))
            price_item.setTextAlignment(Qt.AlignRight)
            self.table.insertRow(self.items)
            self.table.setItem(self.items, 0, description_item)
            self.table.setItem(self.items, 1, price_item)
            self.items += 1
            self.price_temp.append(float(price))
            self.desc_temp.append(desc)
            
         
    @Slot()
    def clear_table(self):
        self.table.setRowCount(0)
        self.items = 0
        self.data = {}
        self.total = 0
        self.update_total()
        
    @Slot()
    def save_data(self):
        if self.items == 0:
            check = self.popup.warning(self, "Warning", "Data Table is empty, if you save now all stored data will be overwritten and deleted. Would you like to continue?",
                               buttons = QMessageBox.StandardButtons(QMessageBox.Yes | QMessageBox.No))
            if check == QMessageBox.Yes:
                data = pd.DataFrame.from_dict(self.data, orient = "index")
                data.to_csv("data.csv")
                print("Yes")
            if check == QMessageBox.No:
                print("No")
                return
                
        else:
            self.data = {}
            self.desc_temp = []
            self.price_temp = []
            for i in range(0, self.table.rowCount()):
                self.desc_temp.append(self.table.item(i, 0).text())
                self.price_temp.append(float(self.table.item(i, 1).text()))
            for i in range(0, len(self.desc_temp)):
                self.data.update({self.desc_temp[i]: self.price_temp[i]})
            
            data = pd.DataFrame.from_dict(self.data, orient = "index")
            data.to_csv("data.csv")
        
        print(self.data)
        
    @Slot()
    def delete_item(self):
        items = self.table.selectionModel().selectedRows()
        for i in items:
            self.table.removeRow(i.row())
            
        self.update_total()
            
                
            
    @Slot()
    def edit_item(self):
        row = self.table.currentRow()
        column = self.table.currentColumn()
        self.id = self.table.item(row, column)
        self.table.editItem(self.id)
        
        self.total = 0    
        for i in range(self.table.rowCount()):
            self.total += float(self.table.item(i, 1).text())
            
        self.update_total()
        
        
class MainWindow(QMainWindow):
    def __init__(self, widget):
        QMainWindow.__init__(self)
        self.setWindowTitle("Expenses Tool")
        
        # Creating the Menu
        self.menu = self.menuBar()
        self.file_menu = self.menu.addMenu("File")
        
        # Adding Exit function
        exit_action = QAction("Exit", self)
        exit_action.setShortcut("Ctrl+Q")
        exit_action.triggered.connect(self.exit_app)
        self.file_menu.addAction(exit_action)
        self.setCentralWidget(widget)
        
    # First Signal/Slot Connection
    @Slot()
    def exit_app(self, checked):
        QApplication.quit()

if __name__ == "__main__":
    
    app = QApplication(sys.argv)
    widget = Widget()
    window = MainWindow(widget)
    window.resize(1600, 1200)
    window.show()

    sys.exit(app.exec_())