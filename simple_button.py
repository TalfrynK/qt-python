# -*- coding: utf-8 -*-
"""
Created on Wed Jan 22 14:49:25 2020

@author: avine
"""

#Simple Button

import sys
from PySide2.QtWidgets import QApplication, QPushButton
from PySide2.QtCore import Slot

# Greetings - function to log messages to the console for error checking and debugging
@Slot() # The @Slot() is a decorator that identifies a function as a slot
def say_hello():
    print("Button clicked! Hello!")
    
# Creating the Qt App
app = QApplication(sys.argv)

# Creating the button
button = QPushButton("Click Me!")
# The QPushButton has a predefined signal called clicked, which is triggered every time the button is clicked. 

# Connecting button click to the function
button.clicked.connect(say_hello)

# Show the button
button.show()

# Run the app
app.exec_()